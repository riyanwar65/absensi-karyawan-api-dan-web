<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // DB::table('karyawan')->insert([
        //     'k_nip'=>'1703087','k_nama'=>'Riyanwar Setiadi',
        //     'k_posisi'=>'Admin','k_alamat'=>'Desa Getrakmoyan',
        //     'k_foto'=>'NULL','k_role'=>'admin','password'=>Hash::make('riyanwar65'),'k_tokenauth'=>'NULL','k_telp'=>'083120615229','k_uuid_device'=>'NULL','k_email'=>'riyanwarsetiadi0605@gmail.com','k_status'=>'aktif']
        // );
        DB::table('karyawan')->where('k_nip','1703087')->update([
            'password'=>Hash::make('anwar6599')
        ]);
    }
}
