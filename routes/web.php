<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

Route::post('/simpanregisteradmin','RegisterController@create')->name('simpanregisteradmin');
Route::post('/proseslogin','LoginController@proseslogin')->name('proseslogin');
Route::group(['middleware' => ['masihlogin']], function () {
    Route::get('/register',function(){
        return view('auth.register');
    })->name('register');
    Route::get('/login',function(){
        return view('auth.login');
    })->name('login');
    Route::get('/',function(){
        return view('auth.login');
    });
});
Route::get('/logout',function(){
    Session::flush();
    return redirect(route('login'));
})->name('logout');

Route::group(['middleware' => ['akses-admin']], function () {
    Route::get('/dashboard', 'AbsensiController@index')->name('dashboard');
    Route::get('/kantor','AbsensiController@kantor')->name('kantor');
    Route::get('/absensi','AbsensiController@absensi');
    Route::get('/karyawan','AbsensiController@karyawan')->name('karyawan');
    Route::get('/aplikasi','AbsensiController@aplikasi');
    Route::get('/tambahkantor','AbsensiController@tambahkantor');
    Route::get('/tambahkaryawan','AbsensiController@tambahkaryawan');
    Route::get('/email','AbsensiController@email');
    Route::post('/prosestambahkaryawan','AbsensiController@prosestambahkaryawan');
    Route::post('/simpankantor','AbsensiController@simpankantor');
    Route::get('/editjarakmaximal/{id_kan}','AbsensiController@editjarakkantor')->name('editjarakmaximal');
    Route::get('/settingjam','AbsensiController@settingjamkerjaget')->name('settingjamkerja');
    Route::post('/simpanjam','AbsensiController@simpanjam')->name('simpanketentuan');
    Route::get('/kententuan/{id}','AbsensiController@editketentuanjam')->name('editketentuanjam');
    Route::post('/updatejarak',function(Request $request){
        DB::table('kantor')->where('kan_id',$request->id_kan)->update([
            'kan_jarak_maximal'=>$request->jarak
        ]);
        return redirect(route('kantor'));
        // dd($request->all());
    })->name('updatejarak');

    Route::post('/updatejam/{id}', function (Request $request,$id) {
        DB::table('jam_kerja')->where('jk_id',$id)->update([
            'jk_batas_masuk'=>$request->jam_masuk,
            'jk_batas_keluar'=>$request->jam_keluar
        ]);

        return redirect(route('settingjamkerja'))->with('alert-success','Data berhasil di update');
    })->name('updatejam');
    Route::get('/hapusjam/{id}',function ($id){
        DB::table('jam_kerja')->where('jk_id',$id)->delete();
        return redirect(route('settingjamkerja'))->with('alert-error','Data Berhasil di hapus');
    })->name('hapusjam');

    Route::get('/rekap','AbsensiController@rekap')->name('rekap');
    Route::get('/profile/{nip}',function($nip){
        $karyawan = DB::table('karyawan')->where('k_nip',$nip)->first();

        return view('contents.profile',compact('karyawan'));
    })->name('profile');

    Route::get('/editprofile/{nip}',function($nip){
        $karyawan = DB::table('karyawan')->where('k_nip',$nip)->first();

        return view('contents.editprofil',compact('karyawan'));
    })->name('editprofil');

    Route::post('password', function (Request $request) {
        DB::table('karyawan')->where('k_nip',$request->nip)->update([
            'password'=>Hash::make($request->password)
        ]);

        return redirect()->back();
    })->name('updatepassword');

    Route::put('/updateprofile/{nip}','AbsensiController@updateprofil')->name('updateprofil');
    Route::get('/resetuuid/{nip}','AbsensiController@resetuuid')->name('resetuuid');
    Route::get('/resetpassword/{nip}','AbsensiController@resetpassword')->name('resetpassword');
});
// Route::get('/deleteuser/{nip}',function ($nip){
//     DB::table('karyawan')->where('k_nip',$nip)->delete();
// })->name('deleteuser');

