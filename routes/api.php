<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/search','AbsensiApiController@searchkaryawan')->name('search');
Route::post('/login','LoginApiController@login')->name('login');
Route::post('/updateuuid','AbsensiApiController@updateuuid');
// Route::group(['middleware' => ['jwtauth']], function () {
   Route::post('/absensi','AbsensiApiController@absensi')->name('absensi');
   Route::post('/updatediri','AbsensiApiController@updatedatadiri')->name('updatedatadiri');
   Route::get('/kantor','AbsensiApiController@kantor')->name('kantor');
   Route::post('/historyabsensi','AbsensiApiController@historyabsensi');
   Route::get('/getsettinganjam','AbsensiApiController@getsettinganjam');
   Route::post('/historyperhari','AbsensiApiController@historyperhari');
   Route::post('/ubahpassword',function(Request $request){
        DB::table('karyawan')->where('k_nip',$request->k_nip)->update([
            'password'=>Hash::make($request->password)
        ]);
        
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>'Password Berhasil diganti'
        ]);
   });
   Route::get('/rekap/{nip}','AbsensiApiController@rekap')->name('rekap');
// });
