@extends('master')
@section('title','Karyawan')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
      @if ($message = Session::get('alert-success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
      @elseif ($message = Session::get('alert-error'))
        <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
      @endif
      <div class="x_panel">
        <div class="x_title">
          <h2>List Karyawan <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <a href="/tambahkaryawan" class="btn btn-info">Tambah</a>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                  <div class="card-box table-responsive">
          <p class="text-muted font-13 m-b-30">
            Data-data list Karyawan
          </p>
          <table id="datatable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>NIP</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Posisi</th>
                <th>Alamat</th>
                <th>Email</th>
                <th>Status</th>
                <th>Role</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($karyawan as $kar)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{$kar->k_nip}}</td>
                        <td>{{$kar->k_nama}}</td>
                        <td>{{ucfirst($kar->jenis_kelamin)}}</td>
                        <td>{{$kar->k_posisi}}</td>
                        <td>{{$kar->k_alamat}}</td>
                        <td>{{$kar->k_email}}</td>
                        <td>{{ucfirst($kar->k_status)}}</td>
                        <td>{{ucfirst($kar->k_role)}}</td>
                        <td><a href="{{route('resetuuid',['nip'=>$kar->k_nip])}}" class="btn btn-danger btn-sm" title="Reset Uuid"><i class="fa fa-mobile"></i></a>
                        <a href="{{route('resetpassword',['nip'=>$kar->k_nip])}}" class="btn btn-warning btn-sm" title="Reset Password"><i class="fa fa-key"></i></a></td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
        </div>
    </div>
  </div>
  </div></div></div>
@endsection
