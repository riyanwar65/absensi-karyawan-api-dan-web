@extends('master')
@section('title','Setting Jam Kantor')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
      @if ($message = Session::get('alert-success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
      @elseif ($message = Session::get('alert-error'))
        <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
      @endif
      <div class="x_panel">
        <div class="x_title">
          <h2>Settingan Jam Kerja <small></small></h2>
          <ul class="nav navbar-right panel_toolbox">
            @if ($cek>0)

            @else
                {{-- <a href="/tambahketentuanjam" class="btn btn-info">Tambah</a> --}}
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Tambah
                </button>
            @endif
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                  <div class="card-box table-responsive">
          <p class="text-muted font-13 m-b-30">
           Settingan Jam Kerja
          </p>
          <table id="datatable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Batas Jam Masuk</th>
                <th>Batas Jam Keluar</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach ($jam as $jk)
                  <tr>
                    <td>{{$no++}}</td>
                    <td>{{date('H:i',strtotime($jk->jk_batas_masuk))}}</td>
                    <td>{{date('H:i',strtotime($jk->jk_batas_keluar))}}</td>
                    <td>
                        <a href="{{route('editketentuanjam',['id'=>$jk->jk_id])}}" class="btn btn-sm btn-warning" title="Edit data"><i class="fa fa-edit"></i></a>
                        <a href="{{route('hapusjam',['id'=>$jk->jk_id])}}" class="btn btn-sm btn-danger" title="Hapus data"><i class="fa fa-trash"></i></a>
                        {{-- <a href="{{route('editjarakmaximal',['id_kan'=>$kn->kan_id])}}" class="btn btn-sm btn-warning" title="Edit Jarak Maximal"><i class="fa fa-edit"></i></a> --}}
                    </td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </div>
    </div>
  </div>
  </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Ketentuan Jam Kerja</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('simpanketentuan')}}" method="post">
            <div class="modal-body">
                @csrf
                <label for="">Jam Masuk</label>
                <input id="timepicker" width="276" class="form-control" name="jam_masuk"/>
                <label for="">Jam Keluar</label>
                <input id="timepicker2" width="276" class="form-control" name="jam_keluar"/>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@push('script')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <script>
    $('#timepicker').timepicker({
        uiLibrary: 'bootstrap4'
    });
    $('#timepicker2').timepicker({
        uiLibrary: 'bootstrap4'
    });
</script>

@endpush
@endsection
