@extends('master')
@section('title','Tambah Karyawan')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="x_panel">
        <div class="x_title">
        <h2>Edit Profile</h2>
        <ul class="nav navbar-right panel_toolbox">
            <a href="{{route('profile',['nip'=>$karyawan->k_nip])}}" class="btn btn-success">Profile</a>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <br />
        <form method="POST" action="{{route('updateprofil',['nip'=>$karyawan->k_nip])}}" enctype="multipart/form-data">
            {{method_field('PUT')}}
            {{ csrf_field() }}
            {{-- <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NIP<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="first-name"  name="nip" class="form-control" autocomplete="disable" value="{{$karyawan->k_nip}}">
                </div>
            </div> --}}
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="first-name"  name="nama" class="form-control" autocomplete="disable" value="{{$karyawan->k_nama}}">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Posisi<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="first-name"  name="posisi" class="form-control" autocomplete="disable" value="{{$karyawan->k_posisi}}">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Telp<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="number" id="first-name"  name="notelp" class="form-control" autocomplete="disable" min="0" value="{{$karyawan->k_telp}}">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                   <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-sm btn-warning">Change Your Password</button>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="email" id="first-name"  name="email" class="form-control" autocomplete="disable" value="{{$karyawan->k_email}}">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Foto Profil<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="file" name="profil" id="" class="form-control" accept=".jpg,.png,.jpeg">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <textarea name="alamat" id="" cols="30" rows="10" class="form-control">{{$karyawan->k_alamat}} </textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"></label>
                <div class="col-md-6 col-sm-6 ">
                    <button class="btn btn-success" name="submit" type="submit">Update</button>
                </div>
            </div>
        </form>
        </div>
    </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Ketentuan Jam Kerja</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('updatepassword')}}" method="post">
            <div class="modal-body">
                @csrf
                <input type="hidden" name="nip" value="{{$karyawan->k_nip}}">
                <input type="password" class="form-control" name="password" placeholder="Masukan Password"/>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update Password</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection
