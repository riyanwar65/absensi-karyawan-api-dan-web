@extends('master')
@section('title','Tambah Karyawan')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="x_panel">
        <div class="x_title">
        <h2>Form Design <small>different form elements</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <a href="/karyawan" class="btn btn-success">List Karyawan</a>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="/prosestambahkaryawan" enctype="multipart/form-data">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NIP<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="first-name" required="required" name="nip" class="form-control" autocomplete="disable">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="first-name" required="required" name="nama" class="form-control" autocomplete="disable">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Posisi<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="first-name" required="required" name="posisi" class="form-control" autocomplete="disable">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Telp<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="number" id="first-name" required="required" name="notelp" class="form-control" autocomplete="disable" min="0">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="password" id="password" required="required" name="password" class="form-control" autocomplete="disable" readonly>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="email" id="first-name" required="required" name="email" class="form-control" autocomplete="disable">
                </div>
            </div>
             <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jenis Kelamin<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <label class="radio inline">
                        <input type="radio" name="jenis_kelamin" value="pria" checked>
                        <span> Pria </span>
                    </label>
                    <label class="radio inline">
                        <input type="radio" name="jenis_kelamin" value="wanita">
                        <span>Wanita</span>
                    </label>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <textarea name="alamat" id="" cols="30" rows="10" class="form-control"></textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"></label>
                <div class="col-md-6 col-sm-6 ">
                    <button class="btn btn-success" name="submit" type="submit">Simpan</button>
                </div>
            </div>
        </form>
        </div>
    </div>
    </div>
</div>

<script>
    let r = Math.random().toString(36).substring(3);
    console.log("random", r);
    document.getElementById("password").value = r;
</script>
@endsection
