@extends('master')
@section('title','Absensi')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Data Absensi<small>Karyawan</small></h2>
          <ul class="nav navbar-right panel_toolbox">
                <a href="{{route('rekap')}}" class="btn btn-sm btn-success"><i class="fa fa-print"> Rekap</i></a>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                  <div class="card-box table-responsive">
          <table id="datatable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Cek in</th>
                <th>Cek Out</th>
                <th>Keterangan Checkin</th>
                <th>Keterangan Checkout</th>
                <th>Status Keterangan</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($absensi as $ab)
                  <tr>
                    <td>{{$no++}}</td>
                    <td>{{$ab->k_nama}}</td>
                    <td>{{$ab->checkin_date." ".date('H:i',strtotime($ab->checkin_time))}}</td>
                    @if ($ab->checkout_time==NULL)
                    <td> - </td>
                    @else
                    <td>{{$ab->checkout_date." ".date('H:i',strtotime($ab->checkout_time))}}</td>
                    @endif
                    @if ($ab->keterangan_checkin==NULL && $ab->keterangan_checkout==NULL)
                    <td> - </td>
                    <td> - </td>
                    @else
                    <td>{{$ab->keterangan_checkin}}</td>
                    <td>{{$ab->keterangan_checkout}}</td>
                    @endif
                    <td>{{$ab->kehadiran}}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </div>
    </div>
  </div>
  </div></div></div>
@endsection
