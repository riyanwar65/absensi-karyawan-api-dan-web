@extends('master')
@section('title','Tambah Lokasi Kantor')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="x_panel">
        <div class="x_title">
        <h2>List Kantor<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <a href="/kantor" class="btn btn-success">List Kantor</a>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <br />
        <form id="lokasi" data-parsley-validate class="form-horizontal form-label-left">
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Kantor<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="namakantor" required="required" name="namakantor" class="form-control" autocomplete="disable">
                </div>
                <div class="col-md-6 col-sm-6 ">
                    <button type="submit" class="btn btn-sm btn-success">Cari</button>
                </div>
            </div>
        </form>
        <div id="kordinatkantor">

        </div>
        </div>
    </div>
    </div>
</div>

@push('script')
    {{-- <script src="https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyC7YFlGI7e7ka0Nb0rjuByg65q2bVDd_hc"></script> --}}
    {{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
    <script src="{{asset('js/axios.min.js')}}"></script>
    <script>
        var formlokasi = document.getElementById('lokasi');
        formlokasi.addEventListener('submit',geocode);
        function geocode(e) {
            e.preventDefault();
            var location = document.getElementById('namakantor').value;
            axios.get('https://maps.googleapis.com/maps/api/geocode/json',{
                params:{
                    address: location,
                    key: 'AIzaSyC7YFlGI7e7ka0Nb0rjuByg65q2bVDd_hc',
                }
            }).then((response) => {
                var alamat = response.data.results[0].formatted_address;
                var longitude = response.data.results[0].geometry.location.lng;
                var latitude = response.data.results[0].geometry.location.lat;
                var output = `
                    <hr>
                    <form id="" data-parsley-validate class="form-horizontal form-label-left" action="/simpankantor" method="POST">
                    @csrf
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Kantor<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="" required="required" name="namakantor" class="form-control" autocomplete="disable" value="${location}" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="" required="required" name="alamat" class="form-control" autocomplete="disable" value="${alamat}" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="" required="required" name="email" class="form-control" autocomplete="disable" placeholder="Harap Masukan Email Kantor">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jarak Maximal<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="number" required="required" name="jarak" class="form-control" min="0" >
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Longitude<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="" required="required" name="longitude" class="form-control" autocomplete="disable" value="${longitude}" readonly>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">latitude<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="" required="required" name="latitude" class="form-control" autocomplete="disable" value="${latitude}" readonly> <br>
                                <button type="submit" class="btn btn-info btn-sm">Simpan</button>
                            </div>
                        </div>
                    </form>
                `;
                document.getElementById('kordinatkantor').innerHTML = output;
            }).catch((error)=>{
                console.log(error);
            })
        }
    </script>
@endpush
@endsection
