@extends('master')
@section('title','Edit Ketentuan')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="x_panel">
        <div class="x_title">
        <h2>Form Design <small>different form elements</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <a href="/settingjam" class="btn btn-success"><< Setting Jam</a>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{route('updatejam',['id'=>$jam->jk_id])}}" enctype="multipart/form-data">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jam Masuk<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input id="timepicker" width="276" class="form-control" name="jam_masuk" value="{{date('H:i',strtotime($jam->jk_batas_masuk))}}"/>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jam Keluar<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input id="timepicker2" width="276" class="form-control" name="jam_keluar" value="{{date('H:i',strtotime($jam->jk_batas_keluar))}}"/>
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </div>
        </form>
        </div>
    </div>
    </div>
</div>
@push('script')
<script
src="https://code.jquery.com/jquery-3.4.1.min.js"
integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
  $('#timepicker').timepicker({
      uiLibrary: 'bootstrap4'
  });
  $('#timepicker2').timepicker({
      uiLibrary: 'bootstrap4'
  });
</script>
@endpush
@endsection
