@extends('master')
@section('title','Absensi')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <div class="x_panel">
        <div class="x_title">
        <h2>List Kantor<small></small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <a href="/kantor" class="btn btn-success">List Kantor</a>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <br />
        <form action="{{route('updatejarak')}}" method="POST">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jarak Kantor<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <input type="hidden" name="id_kan" value="{{$jarak_kantor->kan_id}}">
                    <input type="number" required="required" name="jarak" class="form-control" min="0" value="{{$jarak_kantor->kan_jarak_maximal}}">
                </div>
                <div class="col-md-6 col-sm-6 ">
                    <button type="submit" class="btn btn-sm btn-success">Update</button>
                </div>
            </div>
        </form>
        <div id="kordinatkantor">

        </div>
        </div>
    </div>
    </div>
</div>
@endsection
