@extends('master')
@section('title','Profile')
@section('content')
<div class="x_panel">
    <div class="x_content">
        <div class="col-md-12 col-sm-12  text-center">
            <br>
            <img src="{{asset('Foto Profil/'.$karyawan->k_foto)}}" alt="" class="img-circle img-fluid"> <br><br>
            Nama : {{$karyawan->k_nama}} <br><br>
            NIP  : {{$karyawan->k_nip}} <br><br>
            Posisi : {{$karyawan->k_posisi}} <br><br>
            Jenis Kelamin : {{ucfirst($karyawan->jenis_kelamin)}} <br><br>
            Alamat : {{$karyawan->k_alamat}} <br><br>
            <a href="{{route('editprofil',['nip'=>$karyawan->k_nip])}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit Profile</a>
        </div>
    </div>
</div>
@endsection
