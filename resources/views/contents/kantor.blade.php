@extends('master')
@section('title','Kantor')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 ">
      @if ($message = Session::get('alert-success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
      @elseif ($message = Session::get('alert-error'))
        <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
      @endif
      <div class="x_panel">
        <div class="x_title">
          <h2>List Kantor <small>Users</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <a href="/tambahkantor" class="btn btn-info">Tambah</a>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                  <div class="card-box table-responsive">
          <p class="text-muted font-13 m-b-30">
            Merupakan list kantor yang terdaftar
          </p>
          <table id="datatable" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kantor</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>Longitude</th>
                <th>Latitude</th>
                <th>Jarak Maximal Absensi</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach ($kantor as $kn)
                  <tr>
                    <td>{{$no++}}</td>
                    <td>{{$kn->kan_nama}}</td>
                    <td>{{$kn->kan_email}}</td>
                    <td>{{$kn->kan_alamat}}</td>
                    <td>{{$kn->longitude}}</td>
                    <td>{{$kn->latitude}}</td>
                    <td>{{$kn->kan_jarak_maximal." m"}}</td>
                    <td>
                        <a href="{{route('editjarakmaximal',['id_kan'=>$kn->kan_id])}}" class="btn btn-sm btn-warning" title="Edit Jarak Maximal"><i class="fa fa-edit"></i></a>
                    </td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </div>
    </div>
  </div>
  </div></div></div>
@endsection
