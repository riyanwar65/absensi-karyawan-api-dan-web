<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <title>Laporan Rekap Absensi Karyawan</title>
</head>
<body>
    <center>
        <h1>Laporan Rekap Absensi Karyawan</h1>
    </center>
    <table border="1" width=100%>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Checkin</th>
            <th>CheckOut</th>
            <th>Keterangan Checkin</th>
            <th>Keterangan Checkout</th>
            <th>Keterangan Kehadiran</th>
            <th>Status Keterangan</th>
        </tr>
        @foreach ($absensi as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->k_nama}}</td>
            <td>{{\Carbon\Carbon::parse($item->checkin_date)->format('d, M Y')." ".date('H:i',strtotime($item->checkin_time))}}</td>
            @if ($item->checkout_time==NULL)
            <td> - </td>
            @else
            <td>{{\Carbon\Carbon::parse($item->checkout_date)->format('d, M Y')." ".date('H:i',strtotime($item->checkout_time))}}</td>
            @endif
            @if ($item->keterangan_checkin==NULL && $item->keterangan_checkout==NULL)
            <td> - </td>
            <td> - </td>
            @else
            <td>{{$item->keterangan_checkin}}</td>
            <td>{{$item->keterangan_checkout}}</td>
            @endif
            @if ($item->keterangan_kehadiran==NULL)
            <td> - </td>
            @else
            <td>{{$item->keterangan_kehadiran}}</td>
            @endif
            <td>{{$item->kehadiran}}</td>
        </tr>
        @endforeach
    </table>
</body>
</html>
