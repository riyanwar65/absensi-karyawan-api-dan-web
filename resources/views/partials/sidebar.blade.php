<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
      <div class="navbar nav_title" style="border: 0;">
        <a href="/dashboard" class="site_title"><span>Admin Absensi Mobile</span></a>
      </div>

      <div class="clearfix"></div>

      <!-- menu profile quick info -->
      {{-- <div class="profile clearfix">
        <div class="profile_pic">
          <img src="images/img.jpg" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
          <span>Welcome,</span>
          <h2>John Doe</h2>
        </div>
      </div> --}}
      <!-- /menu profile quick info -->

      <br />

      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
        <ul class="nav side-menu">
            <li><a href="/dashboard"><i class="fa fa-home"></i> Dashboard </a></li>
            <li><a href="/kantor"><i class="fa fa-briefcase"></i> Kantor </a></li>
            <li><a href="/absensi"><i class="fa fa-edit"></i> Absensi </a></li>
            <li><a href="/karyawan"><i class="fa fa-users"></i> Karyawan </a></li>
            <li><a href="/settingjam"><i class="fa fa-cog"></i> Setting Ketentuan Jam </a></li>
            {{-- <li><a href="/aplikasi"><i class="fa fa-tablet"></i> Aplikasi </a></li> --}}
        </ul>
        </div>
        </div>
      <!-- /sidebar menu -->

      <!-- /menu footer buttons -->
      <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Setting Ketentuan Jam" href="/settingjam">
            <span class="fa fa-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Kantor" href="/kantor">
          <span class="fa fa-briefcase" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Karyawan" href="/karyawan">
          <span class="fa fa-users" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('logout')}}">
          <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
      </div>
      <!-- /menu footer buttons -->
    </div>
  </div>
