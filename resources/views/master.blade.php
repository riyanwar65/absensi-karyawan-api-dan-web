<!DOCTYPE html>
<html lang="en">
  @include('partials.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('partials.sidebar')

        @include('partials.topnavigasi')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            @if (\Session::has('alert-warning'))
            <div class="row">
            <div class="col-md-12 col-sm-12 ">
             <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ \Session::get('alert-warning') }}</strong>
              </div>
            </div>
            </div>
            @endif
            @yield('content')
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include('partials.footer')
        <!-- /footer content -->
      </div>
    </div>

    @include('partials.script')

  </body>
</html>
