<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
class Karyawan extends Model implements JWTSubject
{
    //
    protected $table = 'karyawan';
    protected $fillable =[
        'k_nip','k_email','password','k_nip',
        'k_posisi','k_alamat','k_telp','k_foto',
        'k_tokenauth','k_role','k_email','k_status','created_date',
        'updated_date','k_uuid_device','jenis_kelamin','k_nama'
    ];
    public $timestamps = false;
    protected $hidden = [
        'password',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
