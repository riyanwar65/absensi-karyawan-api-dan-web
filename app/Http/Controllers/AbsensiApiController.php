<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;
use PDF;
class AbsensiApiController extends Controller
{
    //
    public function absensi(Request $request){
      $now = Carbon::now('Asia/Jakarta');
      $unit = 'm';
      $kantor = DB::table('kantor')->get();
      $jarak = array();
      $tot_kantor = count($kantor);
      for ($i=0; $i < $tot_kantor; $i++) {
            $derajat = rad2deg(acos((sin(deg2rad($kantor[$i]->latitude))*sin(deg2rad($request->latitude))) + (cos(deg2rad($kantor[$i]->latitude))*cos(deg2rad($request->latitude))*cos(deg2rad($kantor[$i]->longitude-$request->longitude)))));
            $kabeh = ($derajat * 111.13384)*1000;
            array_push($jarak,$kabeh);
      }
    //   $request = $request;
    $cek_kehadiran = DB::table('absensi')->where('k_nip',$request->k_nip)->where('checkin_date',$request->date)->first();
      if($cek_kehadiran==NULL&&$request->status_kehadiran!=NULL){
        $status_kehadiran = DB::table('absensi')->where('k_nip',$request->k_nip)->where('checkin_date',$request->date)->where('kehadiran',$request->status_kehadiran)->get();
        if($request->status_kehadiran=="sakit"){
            DB::table('absensi')->insert([
               'k_nip'=>$request->k_nip,
               'checkin_date'=>$request->input('date'),
               'checkin_time'=>$request->input('time'),
               'keterangan_kehadiran'=>$request->keterangan_kehadiran,
               'created_date'=>$now,
               'kehadiran'=>$request->status_kehadiran
            ]);
            return response()->json([
                [
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Data berhasil di simpan'
                ]
            ]);
         }
         if($request->status_kehadiran=="izin"){
            DB::table('absensi')->insert([
               'k_nip'=>$request->k_nip,
               'checkin_date'=>$request->input('date'),
               'checkin_time'=>$request->input('time'),
               'keterangan_kehadiran'=>$request->keterangan_kehadiran,
               'created_date'=>$now,
               'kehadiran'=>$request->status_kehadiran
            ]);
            return response()->json([
                [
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Data berhasil di simpan'
                ]
            ]);
         }
         elseif (count($status_kehadiran)>0) {
           return response()->json([
               [
                   'status'=>true,
                   'code'=>503,
                   'message'=>'Anda tidak bisa mengirim kembali status kehadiran anda'
               ]
           ]);
         }
         else {
           DB::table('absensi')->insert([
               'k_nip'=>$request->k_nip,
               'checkin_date'=>$request->input('date'),
               'checkin_time'=>$request->input('time'),
               'keterangan_kehadiran'=>$request->keterangan_kehadiran,
               'kehadiran'=>$request->status_kehadiran,
               'created_date'=>$now
            ]);
            return response()->json([
                [
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Data berhasil di simpan'
                ]
            ]);
         }
      }if($request->status_kehadiran==NULL){
        $banyak_data = count($jarak);
        $index = null;
        $fix = array();
        $jam_kerja = DB::table('jam_kerja')->first();
        $inputtime = date("Hi",strtotime($request->input('time')));
        $hitunganinput = strtotime($request->input('time'));
        $jkmasukhitungan = strtotime($jam_kerja->jk_batas_masuk);
        $jkpulanghitungan =strtotime($jam_kerja->jk_batas_keluar);
        $jkmasuk = date("Hi",strtotime($jam_kerja->jk_batas_masuk));
        $jkpulang = date("Hi",strtotime($jam_kerja->jk_batas_keluar));
        for ($i = 0; $i <$banyak_data; $i++) {
              if ($jarak[$i]<$kantor[$i]->kan_jarak_maximal) {
                  $index =$i;
                  if(count(DB::table('absensi')->where('k_nip',$request->k_nip)->where('checkin_date',$request->input('date'))->get())>0 &&
                  count(DB::table('absensi')->where('k_nip',$request->k_nip)->where('checkout_date',$request->input('date'))->get())==0) {
                    //   if ($inputtime<$jkpulang) {
                    //       $diff = $hitunganinput - $jkpulanghitungan;
                    //       $jam = floor($diff/(60*60));
                    //       $menit = $diff - $jam *(60*60);
                    //       $checkout = DB::table('absensi')->insert([
                    //           'k_nip' => $request->input('k_nip'),
                    //           'checkout_date'=>$request->input('date'),
                    //           'checkout_time'=>$request->input('time'),
                    //           'keterangan_checkout'=>'Tidak Sesuai dengan batas jam keluar, Waktu anda Checkout: '.$jam." jam ".floor($menit/60)." Menit",
                    //           'kehadiran'=>'Hadir',
                    //           'absensi'=>'checkout',
                    //           'updated_date'=>$now
                    //       ]);
                    //       $tempat=array(
                    //           'status'=>true,
                    //           'code'=>200,
                    //           'kantor'=>$kantor[$i]->kan_nama,
                    //           'jarak'=>$jarak[$i],
                    //           'jam_batas_keluar'=>$jkpulang,
                    //           'keterangan_checkout'=>'Tidak Sesuai dengan batas jam keluar, Waktu anda Checkout: '.$jam." jam ".floor($menit/60)." Menit",
                    //           'message'=>'Anda telah berhasil Checkout at '.$request->input('time')
                    //       );
                    //       array_push($fix,$tempat);
                    //   }
                     if ($inputtime>=$jkpulang){
                          $checkout = DB::table('absensi')->insert([
                              'k_nip' => $request->input('k_nip'),
                              'checkout_date'=>$request->input('date'),
                              'checkout_time'=>$request->input('time'),
                              'keterangan_checkout'=>'Sesuai dengan settingan waktu pulang',
                              'kehadiran'=>'Hadir',
                              'absensi'=>'checkout',
                              'updated_date'=>$now
                          ]);
                          $tempat=array(
                              'status'=>true,
                              'code'=>200,
                              'kantor'=>$kantor[$i]->kan_nama,
                              'jarak'=>$jarak[$i],
                              'jam_batas_keluar'=>$jkpulang,
                              'message'=>'Anda telah berhasil Checkout at '.$request->input('time')
                          );
                          array_push($fix,$tempat);
                      }
                  }else if (count(DB::table('absensi')->where('k_nip',$request->k_nip)->where('checkout_date',$request->input('date'))->get())>0) {
                      $tempat=array(
                          'status'=>true,
                          'code'=>503,
                          'message'=>'Anda tidak dapat melakukan checkin dan checkout lagi untuk hari ini'
                      );
                      array_push($fix,$tempat);
                  }else{
                      if ($inputtime>$jkmasuk) {
                          $diff = $hitunganinput - $jkmasukhitungan;
                          $jam = floor($diff/(60*60));
                          $menit = $diff - $jam *(60*60);
                          $checkin = DB::table('absensi')->insert([
                              'k_nip' => $request->input('k_nip'),
                              'checkin_date'=>$request->input('date'),
                              'checkin_time'=>$request->input('time'),
                              'kehadiran'=>'Hadir',
                              'keterangan_checkin'=>'Telat '.$jam." jam ".floor($menit/60)." menit ",
                              'absensi'=>'checkin',
                              'created_date'=>$now
                          ]);
                          $tempat=array(
                              'status'=>true,
                              'code'=>200,
                              'kantor'=>$kantor[$i]->kan_nama,
                              'jarak'=>$jarak[$i],
                              'jam_batas_masuk'=>$jkmasuk,
                              'kehadiran'=>'Hadir',
                              'keterangan_checkin'=>'Telat '.$jam." jam ".floor($menit/60)." menit ",
                              'message'=>'Anda telah berhasil Checkin at '.$request->input('time')
                          );
                          array_push($fix,$tempat);
                      }else{
                          $diff = $hitunganinput - $jkmasukhitungan;
                          $jam = floor($diff/(60*60));
                          $menit = $diff - $jam *(60*60);
                          $checkin = DB::table('absensi')->insert([
                              'k_nip' => $request->input('k_nip'),
                              'checkin_date'=>$request->input('date'),
                              'checkin_time'=>$request->input('time'),
                              'kehadiran'=>'Hadir',
                              'keterangan_checkin'=>'Anda tepat Waktu '.$jam." jam ".floor($menit/60)." menit ",
                              'absensi'=>'checkin',
                              'created_date'=>$now
                          ]);
                          $tempat=array(
                              'status'=>true,
                              'code'=>200,
                              'kantor'=>$kantor[$i]->kan_nama,
                              'jarak'=>$jarak[$i],
                              'kehadiran'=>'Hadir',
                              'jam_batas_masuk'=>$jkmasuk,
                              'message'=>'Anda telah berhasil Checkin at '.$request->input('time')
                          );
                          array_push($fix,$tempat);
                      }
                  }
              }
        }
        if (isset($index)) {
          return response()->json($fix);
        } else {
          return response()->json([
              [
                  'status'=>false,
                  'code'=>404,
                  'message'=>"Anda Tidak berada pada Jarak Coverage Kantor"
              ]
          ]);
        }
      }if($cek_kehadiran->kehadiran!='Sakit'||$cek_kehadiran->kehadiran!='sakit'||$cek_kehadiran->kehadiran!='Izin'||$cek_kehadiran->kehadiran!='izin'){
          return response()->json([
            [
                'status'=>true,
                'code'=>200,
                'message'=>'Anda Hari ini tercatat kehadiran : '.$cek_kehadiran->kehadiran
            ]
          ]);
      }
    }

    public function searchkaryawan(Request $request){
        $karyawan = DB::table('karyawan')->where('k_nip',$request->input('k_nip'))->first();
        if ($karyawan) {
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'Berhasil',
                'data'=>$karyawan
            ],200);
        }else{
            return response()->json([
                'status'=>false,
                'code'=>404,
                'message'=>'Data Tidak Di Temukan',
            ],200);
        }
    }


    public function updatedatadiri(Request $request){
        $this->validate($request,[
            'k_foto'=>'image|mimes:jpeg,png,gif,jpg,svg,JPG,JPEG,GIF,PNG'
        ]);

        $get = DB::table('karyawan')->where('k_nip',$request->input('k_nip'))->first();
        if($request->has('k_foto')==NULL) {
            $update = DB::table('karyawan')->where('k_nip',$request->input('k_nip'))->update([
                'k_alamat'=>$request->input('k_alamat'),
                'k_nama' =>$request->input('k_nama'),
                'k_telp'=>$request->input('k_telp'),
            ]);
            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>'Data telah di Perbaharui',
            ],200);
        }else if($request->has('k_foto')!=NULL && $request->has(['k_alamat','k_nama','k_telp'])==NULL){
            $file = $request->file('k_foto');
            $path = 'public/Foto Profil/';
            $namefile = $file->getClientOriginalName();
            if ($get->k_foto!=NULL) {
                // $deletFile = File::delete($path.$get->k_foto);
                $file->move($path,$namefile);
                $update = DB::table('karyawan')->where('k_nip',$request->input('k_nip'))->update([
                    'k_foto'=>$namefile,
                ]);
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Foto Profil telah di perbaharui',
                ],200);
            }else{
                $file = $request->file('k_foto');
                $path = 'public/Foto Profil/';
                $namefile = $file->getClientOriginalName();
                $file->move($path,$namefile);
                $update = DB::table('karyawan')->where('k_nip',$request->input('k_nip'))->update([
                    'k_foto'=>$namefile,
                ]);
                return response()->json([
                    'status'=>true,
                    'code'=>200,
                    'message'=>'Data telah di Perbaharui',
                ],200);
            }
        }
    }

    public function historyabsensi(Request $request){
        $history = DB::table('absensi')->where('k_nip',$request->k_nip)->orderBy('created_date','desc')->get();
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>"Data Berhasil di Ambil",
            'data'=>$history
        ],200);
    }

    public function getsettinganjam(){
        $jam=DB::table('jam_kerja')->get();
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>'Data Berhasil di Ambil',
            'data'=>$jam
        ],200);
    }

    public function historyperhari(Request $request){
        $history = DB::table('absensi')->where('k_nip',$request->k_nip)->where('checkin_date',$request->date)->orWhere('checkout_date',$request->date)->get();
        return response()->json([
            'status'=>true,
            'code'=>200,
            'message'=>'Data Berhasil di Ambil',
            'data'=>$history
        ]);

    }

    public function kantor(){
        $kantor = DB::table('kantor')->get();

        return response()->json([
            'status' => true,
            'code' =>200,
            'message'=>'Data Berhasil di ambil',
            'kantor'=>$kantor
        ],200);
    }

    public function updateuuid(Request $request){
        $uuid = DB::table('karyawan')->where('k_nip',$request->k_nip)->first();

        if ($uuid->k_uuid_device!=NULL) {
            return response()->json([
                'code'=>503,
                'status'=>true,
                'message'=>"Device Anda Telah Terdaftar"
            ],200);
        }else{
            DB::table('karyawan')->where('k_nip',$request->k_nip)->update([
                'k_status'=>'aktif',
                'k_uuid_device'=>$request->k_uuid,
                'k_tokenauth'=>$request->token_auth
            ]);

            return response()->json([
                'status'=>true,
                'code'=>200,
                'message'=>"Device anda berhasil didaftarkan"
            ],200);
        }
    }
    
     
    public function rekap($nip){
        $no = 1;
        $absensi = DB::table('absensi')->join('karyawan','karyawan.k_nip','absensi.k_nip')->orderBy('karyawan.k_nama','asc')->where('absensi.k_nip',$nip)->get();
        $pdf = PDF::loadview('rekap',compact('absensi','no'));
    	return $pdf->download('Laporan Rekap Absensi.pdf');
    }
}
