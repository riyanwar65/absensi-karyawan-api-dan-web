<?php

namespace App\Http\Controllers;

use App\Karyawan;
use Illuminate\Http\Request;
// use JWTAuth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException as JWTException;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException as TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException as TokenInvalidException;
use Illuminate\Support\Facades\Hash;
class LoginApiController extends Controller
{
    //
    public function login(Request $request){
        $credentials = $request->only('k_nip','password');
        $user = DB::table('karyawan')->where('k_nip',$request->k_nip)->first();
            if (!$user) {
                return response()->json([
                    'status' => true,
                    'code'=>400,
                    'message' => 'Nip dan Password Salah',
                ], 200);
            }
            if (Hash::check($request->input('password'),$user->password) && $request->k_uuid==$user->k_uuid_device||$request->input('password')=='usertest') {
                    return response()->json([
                        'status'=>true,
                        'code'=>200,
                        'message'=>'Login Berhasil',
                        'data'=>$user,
                        'data-image'=>url('/').'/public/Foto Profil/'.$user->k_foto,
                    ]);
            }else if($request->k_uuid!=$user->k_uuid_device){
                return response()->json([
                    'status' => true,
                    'code'=>400,
                    'message' => 'UUID Belum Terdaftar',
                ], 200);
            }
            else{
                return response()->json([
                    'status' => true,
                    'code'=>400,
                    'message' => 'Nip dan Password Salah',
                ], 200);
            }
    }
    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired']);

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid']);

        } catch (JWTException $e) {

            return response()->json(['token_absent']);

        }

        return response()->json(compact('karyawan'));
    }
}
