<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
class LoginController extends Controller
{
    //

    public function proseslogin(Request $request){
       $nip = $request->nip;
       $password = $request->password;
       $data = DB::table('karyawan')->where('k_role','admin')->where('k_role','Admin')->where('k_nip',$nip)->where('k_status','Aktif')->where('k_status','aktif')->first();
    //   dd($data);
       if ($data) {
           if (Hash::check($password,$data->password)) {
               Session::put('role',$data->k_role);
               Session::put('nip',$data->k_nip);
               Session::put('nama',$data->k_nama);
               Session::put('posisi',$data->k_posisi);
               Session::put('email',$data->k_email);
               return redirect(route('dashboard'));
           }else{
               return redirect(route('login'))->with('alert-gagal','Password atau Nip, Salah!');
           }
       }else{
           return redirect(route('login'))->with('alert-gagal','Hanya Admin Yang di Perbolehkan');
       }
    }
}
