<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PDF;
use Illuminate\Support\Facades\Session;
class AbsensiController extends Controller
{
    //
    public function index(){
        return view('contents.dashboard');
    }

    public function email(){
        return view('email');
    }

    public function kantor(){
        $no =1;
        $kantor = DB::table('kantor')->get();
        return view('contents.kantor',compact('kantor','no'));
    }

    public function absensi(){
        // $cobain =Carbon::now('Asia/Jakarta');
        // $penambahan = $cobain->addHour(8);
        // dd(Carbon::parse($penambahan)->format('H:i'));
        $no = 1;
        $absensi = DB::table('absensi')->join('karyawan','karyawan.k_nip','absensi.k_nip')->get();
        return view('contents.absensi',compact('absensi','no'));
    }

    public function karyawan(){
        $no =1;
        $karyawan = DB::table('karyawan')->where('k_role','karyawan')->get();
        return view('contents.karyawan',compact('karyawan','no'));
    }

    public function aplikasi(){
        return view('contents.aplikasi');
    }

    public function tambahkantor(){
        return view('contents.tambahkantor');
    }

    public function tambahkaryawan(){
        return view('contents.tambahkaryawan');
    }

    public function prosestambahkaryawan(Request $request){

        $this->validate($request,[
            'nip'=>'required|unique:karyawan,k_nip',
            'notelp'=>'required|unique:karyawan,k_telp|size:12',
            'email'=>'required|unique:karyawan,k_email',
        ],[
            'size' => 'No. telp harus 12',
            'unique' => ':attribute Sudah ada',
        ]);

        $now = Carbon::now('Asia/Jakarta');
        $email = $request->email;
        $data =  ['nama' => $request->nama, 'nip' => $request->nip,'password'=>$request->password];
        // try{
            Mail::send('email',$data, function ($message) use ($email){
                $message->to($email)->subject('Akun absensi Mobile');
                $message->from('yayangtm6599@gmail.com', 'Admin Aplikasi Absensi Mobile');

            });
                if ($request->jenis_kelamin=='pria') {
                    DB::table('karyawan')->insert([
                        'k_nip' => $request->nip,
                        'k_nama' => $request->nama,
                        'k_posisi'=>$request->posisi,
                        'k_alamat'=>$request->alamat,
                        'k_foto'=>'pria.png',
                        'jenis_kelamin'=>$request->jenis_kelamin,
                        // 'k_tokenauth'=>'NULL',
                        // 'k_uuid_device'=>'NULL',
                        'password'=>Hash::make($request->password),
                        'created_date' => $now,
                        'k_telp'=>$request->notelp,
                        'k_email'=>$request->email,
                        'k_status'=>'Belum Aktif',
                        'k_role'=>'karyawan',
                    ]);
                }else if ($request->jenis_kelamin=='wanita') {
                    DB::table('karyawan')->insert([
                        'k_nip' => $request->nip,
                        'k_nama' => $request->nama,
                        'k_posisi'=>$request->posisi,
                        'k_alamat'=>$request->alamat,
                        'k_foto'=>'wanita.png',
                        'jenis_kelamin'=>$request->jenis_kelamin,
                        // 'k_tokenauth'=>'NULL',
                        // 'k_uuid_device'=>'NULL',
                        'created_date' => $now,
                        'password'=>Hash::make($request->password),
                        'k_telp'=>$request->notelp,
                        'k_email'=>$request->email,
                        'k_status'=>'Belum Aktif',
                        'k_role'=>'karyawan',
                    ]);
                }
            return redirect('/karyawan')->with('alert-success','Akun berhasil terkirim ke email dan Data berhasil disimpan');
    }

    public function simpankantor(Request $request){
        $this->validate($request,[
            'namakantor'=>'unique:kantor,kan_nama'
            // 'nip'=>'required|unique:pgsql.abmob.public.karyawan,k_nip',
            // 'notelp'=>'required|unique:pgsql.abmob.public.karyawan,k_telp|size:12',
            // 'email'=>'required|unique:pgsql.abmob.public.karyawan,k_email',
        ],[
            // 'size' => 'No. telp harus 12',
            'unique' => ':attribute Sudah ada',
        ]);

        DB::table('kantor')->insert([
            'kan_nama'=>$request->namakantor,
            'kan_email'=>$request->email,
            'kan_alamat'=>$request->alamat,
            'longitude'=>$request->longitude,
            'latitude'=>$request->latitude,
            'kan_jarak_maximal'=>$request->jarak
        ]);

        return redirect('/kantor')->with('alert-success','Data Kantor berhasil tersimpan');
    }

    public function editjarakkantor($id_kan){
        $jarak_kantor = DB::table('kantor')->where('kan_id',$id_kan)->first();
        return view('contents.editjarakkantor',compact('jarak_kantor'));
    }

    public function settingjamkerjaget(){
        $jam = DB::table('jam_kerja')->get();
        $no = 1;
        $cek = count($jam);
        return view('contents.settingjamketentuan',compact('jam','no','cek'));
    }

    public function simpanjam(Request $request){
        $masuk = date("H:i",strtotime($request->jam_masuk));
        $keluar = date("H:i",strtotime($request->jam_keluar));

        DB::table('jam_kerja')->insert([
            'jk_batas_masuk'=>$masuk,
            'jk_batas_keluar'=>$keluar
        ]);

        return redirect(route('settingjamkerja'))->with('alert-success','Data Berhasil di simpan');
    }
    public function editketentuanjam($id){
        $jam = DB::table('jam_kerja')->where('jk_id',$id)->first();

        return view('contents.editketentuanjam',compact('jam'));
    }

    public function rekap(){
        $no = 1;
        $absensi = DB::table('absensi')->join('karyawan','karyawan.k_nip','absensi.k_nip')->orderBy('karyawan.k_nama','asc')->get();
        $pdf = PDF::loadview('rekap',compact('absensi','no'));
    	return $pdf->download('Laporan Rekap Absensi.pdf');
    }

    public function updateprofil(Request $request,$nip){
        $gambar = $request->file('profil');
        if ($gambar==NULL) {
            DB::table('karyawan')->where('k_nip',$nip)->update([
                // 'k_nip'=>$request->nip,
                'k_nama'=>$request->nama,
                'k_posisi'=>$request->posisi,
                'k_email'=>$request->email,
                'k_alamat'=>$request->alamat,
                'k_telp'=>$request->notelp
            ]);
        } else {
            $tujuan = "Foto Profil";
            $namagambar = $gambar->getClientOriginalName();
            $gambar->move($tujuan,$namagambar);
            DB::table('karyawan')->where('k_nip',$nip)->update([
                // 'k_nip'=>$request->nip,
                'k_nama'=>$request->nama,
                'k_posisi'=>$request->posisi,
                'k_email'=>$request->email,
                'k_foto'=>$namagambar,
                'k_alamat'=>$request->alamat,
                'k_telp'=>$request->notelp
            ]);
        }

        return redirect(route('profile',['nip'=>$nip]));
    }

    public function resetpassword($nip){
        $password_baru = str_random(10);
        $karyawan = DB::table('karyawan')->where('k_nip',$nip)->first();
        $email = $karyawan->k_email;
        $data =  ['nama' => $karyawan->k_nama, 'nip' => $karyawan->k_nip,'password'=>$password_baru];
        Mail::send('resetpassword',$data, function ($message) use ($email){
                $message->to($email)->subject('Password Baru');
                $message->from('yayangtm6599@gmail.com', 'Admin Aplikasi Absensi Mobile');
        });

        $update = DB::table('karyawan')->where('k_nip',$nip)->update([
            'password'=>Hash::make($password_baru)
        ]);

        return redirect(route('karyawan'))->with('alert-success','Password berhasil di reset dan Password baru berhasil di kirim ke '.$email);
    }
    public function resetuuid($nip){
        
        $reset = DB::table('karyawan')->where('k_nip',$nip)->update([
            'k_uuid_device'=>NULL
        ]);
        $nama = DB::table('karyawan')->where('k_nip',$nip)->first();
        
        return redirect(route('karyawan'))->with('alert-success','UUID Device '.$nama->k_nama.' Berhasil di reset');
    }
}

