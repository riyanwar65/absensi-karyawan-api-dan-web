<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class RegisterController extends Controller
{
    public function create(Request $request){
        $this->validate($request,[
            'nip'=>'required|unique:karyawan,k_nip',
            'notelp'=>'required|unique:karyawan,k_telp|size:12',
            'email'=>'required|unique:karyawan,k_email',
        ],[
            'size' => 'No. telp harus 12',
            'unique' => ':attribute Sudah ada',
        ]);
        $now = Carbon::now('Asia/Jakarta');
        if ($request->fotoprofil == NULL) {
            if ($request->jenis_kelamin=='pria') {
                DB::table('karyawan')->insert([
                    'k_nip' => $request->nip,
                    'k_nama' => $request->nama,
                    'k_posisi'=>$request->posisi,
                    'k_alamat'=>$request->alamat,
                    'k_foto'=>'pria.png',
                    'jenis_kelamin'=>$request->jenis_kelamin,
                    'password'=>Hash::make($request->password),
                    'created_date' => $now,
                    'k_telp'=>$request->notelp,
                    'k_email'=>$request->email,
                    'k_status'=>'Aktif',
                    'k_role'=>'admin',
                ]);
            }else if ($request->jenis_kelamin=='wanita') {
                DB::table('karyawan')->insert([
                    'k_nip' => $request->nip,
                    'k_nama' => $request->nama,
                    'k_posisi'=>$request->posisi,
                    'k_alamat'=>$request->alamat,
                    'k_foto'=>'wanita.png',
                    'jenis_kelamin'=>$request->jenis_kelamin,
                    'created_date' => $now,
                    'password'=>Hash::make($request->password),
                    'k_telp'=>$request->notelp,
                    'k_email'=>$request->email,
                    'k_status'=>'Aktif',
                    'k_role'=>'admin',
                ]);
            }
        }else{
            $gambar = $request->file('fotoprofil');
            $path = 'Foto Profil';
            $namagambar = $gambar->getClientOriginalName();
            $gambar->move($path,$namagambar);
            DB::table('karyawan')->insert([
                    'k_nip' => $request->nip,
                    'k_nama' => $request->nama,
                    'k_posisi'=>$request->posisi,
                    'k_alamat'=>$request->alamat,
                    'k_foto'=>$namagambar,
                    'jenis_kelamin'=>$request->jenis_kelamin,
                    'created_date' => $now,
                    'password'=>Hash::make($request->password),
                    'k_telp'=>$request->notelp,
                    'k_email'=>$request->email,
                    'k_status'=>'Aktif',
                    'k_role'=>'admin',
            ]);
        }

        return redirect(route('login'))->with('alert-success','Registrasi Berhasil Silahkan Login');
    }
}
