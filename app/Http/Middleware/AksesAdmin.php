<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
class AksesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('role')==NULL){
            return redirect(route('login'))->with('alert-gagal','Anda Harus Login Terlebih dahulu');
        }
        if (Session::get('role')!='admin') {
            return redirect(route('login'))->with('alert-gagal','Anda Bukan Admin');
        }
        return $next($request);
    }
}
